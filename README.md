# FlowTree

flowtree is a text flow/fba visualizer for metabolic models

## Usage
```
usage: flowtree.py [-h] -s SBML [-d DEPTH] [-p PLACES] [-z]
                   (-r REACTION | -m METABOLITE)

arguments:
  -h, --help            show this help message and exit
  -s SBML, --sbml SBML  SBML model to display
  -d DEPTH, --depth DEPTH
          max depth to display (default:3)
  -p PLACES, --places PLACES
          number of decimal places to display (default:3)
  -z, --zero    only show/follow zero fluxes
  -r REACTION, --reaction REACTION
                reaction to display
  -m METABOLITE, --metabolite METABOLITE
                metabolite to display
```

## Example
Invoking:
`flowtree.py -s mymodel.xml -r R_PROTL -d 2`

will produce:
```
-[0.000] PROTL
   |-[2.000] atp_c
   |-[0.000] trnapro_c
   |-[6.232] pro_DASH_L_
```

which can be read as: 
> There is no flux through the reaction PROTL. This reactions consumes three metabolites, but it is impossible to produce trnapro_c with the current model/constraints.

Try `-d` with higher numbers to see fluxes that produce those metabolites, and the reactants needed by those reactions, etc.

You may also be interested in the `-z` flag, that shows only those reactions/metabolites with zero flux. Example:

```
flowtree.py -s mymodel.xml -r Biomass  -d 3 -z
-[0.000] Biomass
   |-[0.000] his_DASH_L_c
   |   |-[0.000] HISt
   |-[0.000] chlb_u
   |   |-[0.000] CHLBSP
   |   |-[0.000] GGCHLDBR
   |-[0.000] cys_DASH_L_c
```

which reads:
> You can't produce Biomass, because the model lacks the capacity to produce the metabolites `his_DASH_L_c`, `chlb_u` and `cys_DASH_L_c`. This is because the reactions `HISt, CHLBSP, GGCHLDBR` can't produce flux, and because there is no reaction producing `cys_DASH_L_c`.


## Setup
FlowTree requires [cobrapy](https://github.com/opencobra/cobrapy). See the `requirements.txt` file for other libraries. You should be fine doing:

`pip install -r requirements.txt`


## Developed by

Nicolas Loira: nloira@gmail.com
