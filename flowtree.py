#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
flowtree is a text flow/fba visualizer for metabolic models.
For help, try: python flowtree.py --help
or visit: https://bitbucket.org/nloira/flowtree

Nicolas Loira
nloira@gmail.com
April 2016
"""
from __future__ import print_function

import argparse
import cobra.io
import cobra.flux_analysis
from cobra import Reaction

INDENT = "   |"


def is_zero(x):
	return abs(x) < 0.0001


def fba(model, reaction):
	# my_objective = model.reactions.get_by_id(reactionid)
	model.change_objective(reaction)
	return model.optimize()


def show_flux(prefix, sol, elementid, args):
	out = "%s-[%.*f] %s" % (prefix, args.places, sol.f, elementid)
	print(out)


def flux_reaction(prefix, depth, model, reaction, args):
	sol = fba(model, reaction)

	if args.zero and not is_zero(sol.f):
		return

	show_flux(prefix, sol, reaction.id, args)

	if depth > 1:
		newprefix = prefix + INDENT
		for m in reaction.reactants:
			flux_metabolite(newprefix, depth - 1, model, m, args)


def flux_metabolite(prefix, depth, model, metabolite, args):

	dummy_reactionid = "DUMMY_FLUXTREE"
	dummy_reaction = Reaction(dummy_reactionid)
	model.add_reaction(dummy_reaction)
	dummy_reaction.add_metabolites({metabolite.id: -1.0})
	sol = fba(model, dummy_reaction)
	model.remove_reactions([dummy_reaction])

	if args.zero and not is_zero(sol.f):
		return

	show_flux(prefix, sol, metabolite.id, args)

	if depth > 1:
		newprefix = prefix + INDENT
		reactions = [r for r in model.reactions if metabolite in r.products]
		for r in reactions:
			flux_reaction(newprefix, depth - 1, model, r, args)


def main():
	# args
	parser = argparse.ArgumentParser(description="flowtree is a flow/fba visualizer for metabolic models")
	parser.add_argument("-s", "--sbml", required=True, help="SBML model to display")
	parser.add_argument("-d", "--depth", default=3, type=int, help="max depth to display")
	parser.add_argument("-p", "--places", default=3, type=int, help="number of decimal places to display")
	# parser.add_argument("-S", "--solver", help="cobrapy solver")
	parser.add_argument("-z", "--zero", action="store_true", help="only show/follow zero fluxes")
	# parser.add_argument("-A", "--ANSI", default=True, action=help="use ANSI lines")

	group = parser.add_mutually_exclusive_group(required=True)
	group.add_argument("-r", "--reaction", help="reaction to display")
	group.add_argument("-m", "--metabolite", help="metabolite to display")
	args = parser.parse_args()

	model = cobra.io.read_sbml_model(args.sbml)
	prefix = ""

	if args.reaction is not None:
		reactionid = args.reaction[2:] if args.reaction.startswith("R_") else args.reaction
		reaction = model.reactions.get_by_id(reactionid)
		flux_reaction(prefix, args.depth, model, reaction, args)
	else:
		metaboliteid = args.metabolite[2:] if args.metabolite.startswith("M_") else args.metabolite
		metabolite = model.metabolites.get_by_id(metaboliteid)
		flux_metabolite(prefix, args.depth, model, metabolite, args)


if __name__ == '__main__':
	main()
